//
//  DDService.h
//  WhoWhatWhere
//
//  Created by Olivier THIERRY on 15/09/11.
//  Copyright 2011 Deloitte Online. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DDServiceProtocol.h"
#import "DDServiceResourcePath.h"
#import "DDServiceRequestLoader.h"

DDServiceResourcePath *DDServicePathMake(NSString *path, ...);

@interface DDService : NSObject <DDService>
{
    NSURL               *_URL;
    
    NSOperationQueue    *_queue;
}

// Base URL of the service
@property (nonatomic, retain, readonly) NSURL *URL;

@property (nonatomic, retain, readonly) NSOperationQueue *queue;

- (id)                      initWithURL:(NSURL*)URL;
+ (id)                      serviceWithURL:(NSURL*)URL;

#if NS_BLOCKS_AVAILABLE

/** 
 * Low level loading
 */
- (DDServiceRequestLoader*) load:(DDServiceResourcePath*)resoucePath synchronously:(BOOL)synchronously configuration:(void (^)(DDServiceRequestLoader *loader))configuration;
- (DDServiceRequestLoader*) load:(DDServiceResourcePath *)resoucePath synchronously:(BOOL)synchronously HTTPMethod:(NSString*)HTTPMethod configuration:(void (^)(DDServiceRequestLoader *))configuration;

/** 
 * Hight level asynchronous loading
 */
- (DDServiceRequestLoader*) get:(DDServiceResourcePath*)resoucePath configuration:(void (^)(DDServiceRequestLoader *loader))configuration;
- (DDServiceRequestLoader*) post:(DDServiceResourcePath*)resoucePath configuration:(void (^)(DDServiceRequestLoader *loader))configuration;
- (DDServiceRequestLoader*) put:(DDServiceResourcePath*)resoucePath configuration:(void (^)(DDServiceRequestLoader *loader))configuration;
- (DDServiceRequestLoader*) delete:(DDServiceResourcePath*)resoucePath configuration:(void (^)(DDServiceRequestLoader *loader))configuration;

/** 
 * Hight level synchronous loading
 */
- (DDServiceRequestLoader*) getSynchronously:(DDServiceResourcePath*)resoucePath configuration:(void (^)(DDServiceRequestLoader *loader))configuration;
- (DDServiceRequestLoader*) postSynchronously:(DDServiceResourcePath*)resoucePath configuration:(void (^)(DDServiceRequestLoader *loader))configuration;
- (DDServiceRequestLoader*) putSynchronously:(DDServiceResourcePath*)resoucePath configuration:(void (^)(DDServiceRequestLoader *loader))configuration;
- (DDServiceRequestLoader*) deleteSynchronously:(DDServiceResourcePath*)resoucePath configuration:(void (^)(DDServiceRequestLoader *loader))configuration;


#endif

- (DDServiceRequestLoader*) getLoaderForResourcePath:(DDServiceResourcePath*)resoucePath;

@end
