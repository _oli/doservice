//
//  DDServiceRequestLoader.h
//  WhoWhatWhere
//
//  Created by Olivier THIERRY on 13/09/11.
//  Copyright 2011 Deloitte Online. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "DDServiceProtocol.h"
#import "OTEventDispatcher.h"
#import "DDServiceRequestFactory.h"
#import "NSString+SBJSON.h"

@class DDServiceRequestLoader, DDServiceResourcePath;

@protocol DDServiceRequestLoaderDelegate <NSObject>

- (void) requestLoader:(DDServiceRequestLoader*)loader didLoadObject:(id)object atResourcePath:(DDServiceResourcePath*)path;
- (void) requestLoader:(DDServiceRequestLoader*)loader didFailWithError:(NSError*)error atResourcePath:(DDServiceResourcePath*)path;

@end

@interface DDServiceRequestLoader : NSOperation
{
    id<DDServiceRequestLoaderDelegate>   _delegate;
    
    id<DDService>                        _service;
    
    DDServiceResourcePath               *_resourcePath;
    
    ASIHTTPRequest                      *_request;

    NSDictionary                        *_parameters;
    
    NSString                            *_method;
    
    OTEventDispatcher                   *_eventDispatcher;
}

@property (nonatomic, assign)           id<DDServiceRequestLoaderDelegate>    delegate;

@property (nonatomic, assign)           id<DDService>                         service;

@property (nonatomic, retain, readonly) ASIHTTPRequest                       *request;

@property (nonatomic, copy)             NSDictionary                         *parameters;

@property (nonatomic, retain)           DDServiceResourcePath                *resourcePath;

@property (nonatomic, copy)             NSString                             *method;

@property (nonatomic, readonly)         BOOL                                  isExecuting;
@property (nonatomic, readonly)         BOOL                                  isFinished;
@property (nonatomic, readonly)         BOOL                                  isCanceled;


- (id)        initWithService:(id<DDService>)service andResourcePath:(DDServiceResourcePath*)resourcePath;
+ (id)        loaderForService:(id<DDService>)service andResourcePath:(DDServiceResourcePath*)resourcePath;

- (void)      load;
- (void)      loadSynchronously;

#if NS_BLOCKS_AVAILABLE

- (void)      onStarted:(void(^)(void))event;
- (void)      onSuccess:(void (^)(id object))event;
- (void)      onFailure:(void (^)(NSError *error))event;
- (void)      onCompletion:(void (^)(void))event;

#endif

@end
