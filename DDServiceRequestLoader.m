//
//  DDServiceRequestLoader.m
//  WhoWhatWhere
//
//  Created by Olivier THIERRY on 13/09/11.
//  Copyright 2011 Deloitte Online. All rights reserved.
//

#import "DDServiceRequestLoader.h"

#import "DDServiceResourcePath.h"
#import "NSDictionary+QueryString.h"
#import "NSString+SBJSON.h"
#import "ASIFormDataRequest.h"
#import "DDServiceRequestFactory.h"

@interface DDServiceRequestLoader ()

- (void) _loadSynchronously:(BOOL)synchronously;

- (void) _notifyOperationObserverOperationDidFinish;
- (void) _requestDidFinish;
- (void) _requestDidFail;

- (void) _buildRequest;

@property (nonatomic, retain) ASIHTTPRequest    *request;
@property (nonatomic, retain) OTEventDispatcher *eventDispatcher;
@property (nonatomic)         BOOL               isExecuting;
@property (nonatomic)         BOOL               isFinished;
@property (nonatomic)         BOOL               isCanceled;

@end

@implementation DDServiceRequestLoader

@synthesize request         = _request,
parameters      = _parameters,
delegate        = _delegate,
service         = _service,
resourcePath    = _resourcePath,
eventDispatcher = _eventDispatcher,
method          = _method,
isFinished      = _isFinished,
isExecuting     = _isExecuting,
isCanceled      = _isCanceled;

- (id) initWithService:(id<DDService>)service andResourcePath:(DDServiceResourcePath*)resourcePath
{
    self = [super init];
    
    if (self)
    {
        [self setService:service];
        [self setResourcePath:resourcePath];
        [self setEventDispatcher:[[OTEventDispatcher new] autorelease]];
        [self setMethod:@"GET"];
    }
    
    return self;
}

+ (id) loaderForService:(id<DDService>)service andResourcePath:(DDServiceResourcePath*)resourcePath
{
    return [[[self alloc] initWithService:service andResourcePath:resourcePath] autorelease];
}

- (void)dealloc
{
    [self.request clearDelegatesAndCancel];
    
    [self setDelegate:nil];
    [self setService:nil];
    [self setRequest:nil];
    [self setParameters:nil];
    [self setResourcePath:nil];
    [self setEventDispatcher:nil];
    [self setMethod:nil];
    
    [super dealloc];
}

#pragma mark - Public

- (void) start
{
    // Don't event start the operation if aborted/cancelled
    if (self.isFinished || self.isCanceled)
    {
        return;
    }
    
    [self willChangeValueForKey:@"isExecuting"];
    [self setIsExecuting:YES];
    [self didChangeValueForKey:@"isExecuting"];
    
    [self _loadSynchronously:YES];
}

- (void) cancel
{
    [self.request clearDelegatesAndCancel];
    
    [self willChangeValueForKey:@"isExecuting"];
    [self willChangeValueForKey:@"isCanceled"];
    [self willChangeValueForKey:@"isFinished"];
    
    [self setIsExecuting:NO];
    [self setIsCanceled:YES];
    [self setIsFinished:YES];
    
    [self didChangeValueForKey:@"isCanceled"];
    [self didChangeValueForKey:@"isExecuting"];
    [self didChangeValueForKey:@"isFinished"];
}

- (void) load
{    
    [self _loadSynchronously:NO];
}

- (void) loadSynchronously
{
    [self _loadSynchronously:YES];
}

#if NS_BLOCKS_AVAILABLE

#pragma mark - Public - Event registration

- (void) onStarted:(void(^)(void))event
{
    [self.eventDispatcher setEvent:event forKey:@"on_started"];
}

- (void) onSuccess:(void (^)(id object))event
{
    [self.eventDispatcher setEvent:event forKey:@"on_success"];
}

- (void) onFailure:(void (^)(NSError *error))event
{
    [self.eventDispatcher setEvent:event forKey:@"on_failure"];    
}

- (void) onCompletion:(void (^)(void))event
{
    [self.eventDispatcher setEvent:event forKey:@"on_completion"];
}

#endif

#pragma mark - Private

- (void) _buildRequest
{
    // Build the request
    [self setRequest:[DDServiceRequestFactory requestWithURL:[self.service URLForResourcePath:self.resourcePath]
                                                  parameters:self.parameters
                                                  HTTPMethod:self.method]];
    
    RKLogInfo(@"Building request (%@ | %@) method : %@ | parameters : %@", self.resourcePath.path, self.request.url, self.method, self.parameters);
    
    // Assign delegate
    [self.request setDelegate:self];
    
    // Set callbacks
    [self.request setDidFinishSelector:@selector(_requestDidFinish)];
    [self.request setDidFailSelector:@selector(_requestDidFail)];
    [self.request setDidStartSelector:@selector(_requestDidStart)];
}

- (void) _loadSynchronously:(BOOL)synchronously
{
    [self _buildRequest];
    
    RKLogInfo(@"Start loading request (%@ | %@) synchronously ? %d", self.resourcePath.path, self.request.url, synchronously);
    
    if (synchronously)
    {
        [self.request startSynchronous];
    }
    else
    {
        [self.request startAsynchronous];
    }
}

#pragma mark - Private - Request delegate

- (void) _notifyOperationObserverOperationDidFinish
{
    [self willChangeValueForKey:@"isExecuting"];
    [self willChangeValueForKey:@"isFinished"];
    
    [self setIsExecuting:NO];
    [self setIsFinished:YES];
    
    [self didChangeValueForKey:@"isExecuting"];
    [self didChangeValueForKey:@"isFinished"];   
}

- (void) _requestDidStart
{
    [self.eventDispatcher fire:@"on_started"];
}

- (void) _requestDidFinish
{
    RKLogInfo(@"Response body for %@ : %@", self.resourcePath.path, [self.request responseString]);
    
    // Get the object as a JSON
    id object = [[self.request responseString] JSONValue];
    
    [self.eventDispatcher fire:@"on_success" withObject:object];
    
    // Notify the delegate
    if ([self.delegate respondsToSelector:@selector(requestLoader:didLoadObject:atResourcePath:)])
    {
        [self.delegate requestLoader:self
                       didLoadObject:object
                      atResourcePath:self.resourcePath];
    }   
    
    [self.eventDispatcher fire:@"on_completion"];
    
    [self _notifyOperationObserverOperationDidFinish];
}

- (void) _requestDidFail
{
    [self.eventDispatcher fire:@"on_failure" withObject:self.request.error];
    
    // Notify the delegate
    if ([self.delegate respondsToSelector:@selector(requestLoader:didFailWithError:atResourcePath:)])
    {
        [self.delegate requestLoader:self 
                    didFailWithError:self.request.error
                      atResourcePath:self.resourcePath];
    }
    
    [self.eventDispatcher fire:@"on_completion"];
    
    [self _notifyOperationObserverOperationDidFinish];
}

@end
