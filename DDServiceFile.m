//
//  DDServiceFile.m
//  AusPost
//
//  Created by Philippe THIERRY on 3/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DDServiceFile.h"

@implementation DDServiceFile

@synthesize name        = _name,
            contentType = _contentType,
            data        = _data;

- (id) initWithName:(NSString*)name data:(NSData*)data contentType:(NSString*)contentType
{
    self = [super init];
    
    if (self) 
    {
        [self setName:name];
        [self setData:data];
        [self setContentType:contentType];
    }
    
    return self;
}

+ (id) fileWithName:(NSString*)name data:(NSData*)data contentType:(NSString*)contentType
{
    return [[self alloc] initWithName:name data:data contentType:contentType];
}

@end
