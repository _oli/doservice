//
//  DDServiceFile.h
//  AusPost
//
//  Created by Philippe THIERRY on 3/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DDServiceFile : NSObject

- (id) initWithName:(NSString*)name data:(NSData*)data contentType:(NSString*)contentType;
+ (id) fileWithName:(NSString*)name data:(NSData*)data contentType:(NSString*)contentType;

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *contentType;
@property (nonatomic, copy) NSData   *data;

@end
