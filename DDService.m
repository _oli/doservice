//
//  DDService.m
//  WhoWhatWhere
//
//  Created by Olivier THIERRY on 15/09/11.
//  Copyright 2011 Deloitte Online. All rights reserved.
//

#import "DDService.h"

DDServiceResourcePath *DDServicePathMake(NSString *path, ...)
{
    va_list args;
    va_start(args, path);

    return [DDServiceResourcePath resoucePathWithPath:[[[NSString alloc] initWithFormat:path arguments:args] autorelease]];
}

@interface DDService ()

@property (nonatomic, retain) NSURL            *URL;
@property (nonatomic, retain) NSOperationQueue *queue;

@end

@implementation DDService

@synthesize URL   = _URL,
            queue = _queue;

- (id) initWithURL:(NSURL*)URL
{
    self = [super init];
    
    if (self)
    {
        self.URL   = URL;
        self.queue = [[NSOperationQueue new] autorelease];
    }
    
    return self;
}

+ (id) serviceWithURL:(NSURL*)URL
{
    return [[[self alloc] initWithURL:URL] autorelease];
}

- (void) dealloc
{
    // Cancel all the running operations
    [self.queue cancelAllOperations];
    
    self.URL   = nil;
    self.queue = nil;
    [super dealloc];
}

#pragma - DDService

- (NSURL*) URLForResourcePath:(DDServiceResourcePath*)resourcePath
{
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [self.URL relativeString], resourcePath.path]];
}

#pragma mark - Public API

#if NS_BLOCKS_AVAILABLE

/** 
 * Low level loading
 */
- (DDServiceRequestLoader*) load:(DDServiceResourcePath*)resoucePath synchronously:(BOOL)synchronously configuration:(void (^)(DDServiceRequestLoader *loader))configuration
{
    DDServiceRequestLoader *loader  = [self getLoaderForResourcePath:resoucePath];
    
    if (configuration)
    {
        configuration(loader);
    }    
    
    synchronously ? [loader loadSynchronously] :  [self.queue addOperation:loader];
    
    return loader;    
}

- (DDServiceRequestLoader*) load:(DDServiceResourcePath *)resoucePath synchronously:(BOOL)synchronously HTTPMethod:(NSString*)HTTPMethod configuration:(void (^)(DDServiceRequestLoader *))configuration
{
    return [self load:resoucePath synchronously:synchronously configuration:^(DDServiceRequestLoader *loader) {
        [loader setMethod:HTTPMethod];
        configuration(loader);
    }];    
}

- (DDServiceRequestLoader*) get:(DDServiceResourcePath*)resoucePath configuration:(void (^)(DDServiceRequestLoader *loader))configuration
{
    return [self load:resoucePath synchronously:NO HTTPMethod:@"GET" configuration:configuration];
}

- (DDServiceRequestLoader*) post:(DDServiceResourcePath*)resoucePath configuration:(void (^)(DDServiceRequestLoader *loader))configuration
{
    return [self load:resoucePath synchronously:NO HTTPMethod:@"POST" configuration:configuration];
}

- (DDServiceRequestLoader*) put:(DDServiceResourcePath*)resoucePath configuration:(void (^)(DDServiceRequestLoader *loader))configuration
{
    return [self load:resoucePath synchronously:NO HTTPMethod:@"PUT" configuration:configuration];
}

- (DDServiceRequestLoader*) delete:(DDServiceResourcePath*)resoucePath configuration:(void (^)(DDServiceRequestLoader *loader))configuration
{
    return [self load:resoucePath synchronously:NO HTTPMethod:@"DELETE" configuration:configuration];
}

/** 
 * Hight level synchronous loading
 */
- (DDServiceRequestLoader*) getSynchronously:(DDServiceResourcePath*)resoucePath configuration:(void (^)(DDServiceRequestLoader *loader))configuration
{
    return [self load:resoucePath synchronously:YES HTTPMethod:@"GET" configuration:configuration];
}

- (DDServiceRequestLoader*) postSynchronously:(DDServiceResourcePath*)resoucePath configuration:(void (^)(DDServiceRequestLoader *loader))configuration
{
    return [self load:resoucePath synchronously:YES HTTPMethod:@"POST" configuration:configuration];    
}

- (DDServiceRequestLoader*) putSynchronously:(DDServiceResourcePath*)resoucePath configuration:(void (^)(DDServiceRequestLoader *loader))configuration
{
    return [self load:resoucePath synchronously:YES HTTPMethod:@"PUT" configuration:configuration];
}

- (DDServiceRequestLoader*) deleteSynchronously:(DDServiceResourcePath*)resoucePath configuration:(void (^)(DDServiceRequestLoader *loader))configuration
{
    return [self load:resoucePath synchronously:YES HTTPMethod:@"DELETE" configuration:configuration];
}


#endif

- (DDServiceRequestLoader*) getLoaderForResourcePath:(DDServiceResourcePath*)resoucePath
{
    return [DDServiceRequestLoader loaderForService:self andResourcePath:resoucePath];
}

@end
