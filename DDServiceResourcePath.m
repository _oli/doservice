//
//  DDServiceResourcePath.m
//  WhoWhatWhere
//
//  Created by Olivier THIERRY on 13/09/11.
//  Copyright 2011 Deloitte Online. All rights reserved.
//

#import "DDServiceResourcePath.h"

@implementation DDServiceResourcePath

@synthesize path = _path;

- (id) initWithPath:(NSString*)path
{
    if (self) 
    {
        [self setPath:path];
    }
    
    return self;
}

+ (id) resoucePathWithPath:(NSString*)path
{
    return [[[self alloc] initWithPath:path] autorelease];
}

- (BOOL)isResource:(NSString *)resource
{
    return [self.path rangeOfString:resource].location != NSNotFound;
}

- (void)dealloc
{
    [self setPath:nil];
    [super dealloc];
}

@end
