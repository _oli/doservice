//
//  DDServiceRequestFactory.h
//  DDServiceExample
//
//  Created by Philippe THIERRY on 1/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"

@interface DDServiceRequestFactory : NSObject

+ (ASIHTTPRequest*) requestWithURL:(NSURL*)URL parameters:(NSDictionary*)parameters HTTPMethod:(NSString*)HTTPMethod;

@end
