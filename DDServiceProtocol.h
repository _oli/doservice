//
//  DDServiceProtocol.h
//  WhoWhatWhere
//
//  Created by Olivier THIERRY on 13/09/11.
//  Copyright 2011 Deloitte Online. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DDServiceResourcePath;

@protocol DDService <NSObject>

- (NSURL*) URLForResourcePath:(DDServiceResourcePath*)path;

@end