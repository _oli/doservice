//
//  DDServiceResourcePath.h
//  WhoWhatWhere
//
//  Created by Olivier THIERRY on 13/09/11.
//  Copyright 2011 Deloitte Online. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DDServiceResourcePath : NSObject
{
    NSString  *_path;
}

@property (nonatomic, copy) NSString   *path;

- (id) initWithPath:(NSString*)path;
+ (id) resoucePathWithPath:(NSString*)path;

- (BOOL) isResource:(NSString*)resource;

@end
