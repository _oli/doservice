//
//  DDServiceRequestFactory.m
//  DDServiceExample
//
//  Created by Philippe THIERRY on 1/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DDServiceRequestFactory.h"
#import "NSDictionary+QueryString.h"

@implementation DDServiceRequestFactory

+ (ASIHTTPRequest*) requestWithURL:(NSURL*)URL parameters:(NSDictionary*)parameters HTTPMethod:(NSString*)HTTPMethod;
{
    ASIHTTPRequest *request = nil;
    
    // Init the request depending on HTTP method
    if ([HTTPMethod isEqualToString:@"POST"] || [HTTPMethod isEqualToString:@"PUT"])
    {
        request = [ASIFormDataRequest requestWithURL:URL];
    }
    else
    {
        request = [ASIHTTPRequest requestWithURL:URL];
    }
    
    // Set the HTTP method
    [request setRequestMethod:HTTPMethod];
    
    // Set parameters 
    if ([request.requestMethod isEqualToString:@"POST"] 
     || [request.requestMethod isEqualToString:@"PUT"])
    {
        for (NSString *parameter in [parameters allKeys])
        {
            id value = [parameters objectForKey:parameter];
            
            if ([value isKindOfClass:[NSData class]])
            {
                [(ASIFormDataRequest*)request setData:value withFileName:@"file" andContentType:@"image/jpeg" forKey:parameter];
            }
            else
            {
                [(ASIFormDataRequest*)request setPostValue:value forKey:parameter];                
            }
        }        
    }
    else
    {  
        NSString *currentURLString     = [[request.url relativeString] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSString *replacementURLString = [NSString stringWithFormat:@"%@%@%@", currentURLString, 
                                          [currentURLString rangeOfString:@"?"].location != NSNotFound ? @"&" : @"?",
                                          [parameters stringWithFormEncodedComponents]];
        
        [request setURL:[NSURL URLWithString:replacementURLString]];
    }
    
    return request;
}

@end
